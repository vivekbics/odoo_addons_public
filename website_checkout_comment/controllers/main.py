# -*- coding: utf-8 -*-
from openerp import SUPERUSER_ID
from openerp.http import request
from openerp.addons.website_sale.controllers.main import website_sale
import re

class CheckoutComment(website_sale):

    def checkout_values(self, data=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        sale_order_obj = registry.get('sale.order')
        if data:
            current_order = request.website.sale_get_order(context=context)
            sale_order_obj.write(cr, SUPERUSER_ID, [current_order.id],
                                 {'customer_comment': data.get('customer_comment', None)},
                                 context=context)
        return super(CheckoutComment, self).checkout_values(data)
